﻿using System.Collections;
using Invector.CharacterController;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class SniperController : MonoBehaviour
{
    public float damage = 25f;
    public float damageKill = 50f;
    public Camera FpsCamera;

    [SerializeField] public GameObject impactDirt;

    [SerializeField] public GameObject impactMetal;

    [SerializeField] public GameObject impactStone;
    private Cyclethroughweapon holder;
    [SerializeField] public GameObject impactZombie;
    public LayerMask layerMask;
    [SerializeField] private AudioClip shootSound;
    [SerializeField] private Animator anim;
    private AudioSource audioSource;
    private bool isShooting;
    [SerializeField] private ParticleSystem muzzleObject;
    [SerializeField] private ScopeManager scopeManager;
    [SerializeField] private GameObject ZoomButton;
    [SerializeField] private GameObject bulletObject;
    [SerializeField] private vThirdPersonController player;
    private Camera cameraBullet;
    [SerializeField] private Camera playerCamera;
    [SerializeField] private Camera WeaponCamera;
    [SerializeField] private GameObject targetManager;
    public GameObject bullet;
    private TargetController targetController;
    private IEnumerator coroutine;
    private Vector3 hitpoint;
    [SerializeField] private Canvas canvas;
    public void ShotMobile()
    {
        if (!isShooting)
        {
            StartCoroutine("Shoot");
        }
    }

    void Start()
    {
        holder = GetComponentInParent<Cyclethroughweapon>();
        audioSource = this.GetComponent<AudioSource>();
    }
    // Use this for initialization
    private void OnEnable()
    {
        isShooting = false;
        ZoomButton.SetActive(true);
    }
    // Use this for initialization
    private void OnDisable()
    {
        if (ZoomButton!=null)
        {
            ZoomButton.SetActive(false);
        }
    }

    void ShootBullet(TargetController target)
    {
        if (scopeManager.isScoped)
        {
            scopeManager.ScopeInAndOut();
        }
        Time.timeScale = 0.01f;
        canvas.enabled = false;
        targetController = target;
        scopeManager.enabled = false;
        targetManager.SetActive(false);
        
        bullet = Instantiate(bulletObject, muzzleObject.transform.position, muzzleObject.transform.rotation);
        Debug.Log(bullet);
        cameraBullet = bullet.gameObject.transform.GetChild(0).GetComponent<Camera>();
        Debug.Log(cameraBullet);
        cameraBullet = Camera.main;
        playerCamera.enabled = false;
        WeaponCamera.enabled = false;
       
        Debug.Log(Time.timeScale);
        player.enabled = false;
        audioSource.pitch = 0.5f;
      
    }
       
    

    private IEnumerator Shoot()
    {
        isShooting = true;

        audioSource.PlayOneShot(shootSound);
        player.animator.SetBool("Shoot",true);
        muzzleObject.Play();

        RaycastHit hit;
        if (Physics.Raycast(FpsCamera.transform.position, FpsCamera.transform.forward, out hit, Mathf.Infinity,
            layerMask))
        {
            Debug.Log(hit.collider.transform.tag);
            var target = hit.transform.GetComponent<TargetController>();
            if (target != null && hit.collider.transform.CompareTag("ZombieKill"))
            {
                float RandomShoot = Random.Range(0f, 1f);
                hitpoint = hit.collider.transform.position;
                if (RandomShoot < 0.3f)
                {
                   // ShootBullet(target);
                   target.TakeDamage(damageKill);
                }
                else
                {
                    target.TakeDamage(damageKill);
                }
            }
            else if (target != null && hit.collider.transform.CompareTag("Zombie"))
            {
                target.TakeDamage(damage);               
            }
            if (hit.collider.transform.CompareTag("Zombie") || hit.collider.transform.CompareTag("ZombieKill"))
            {
                var impactGo = Instantiate(impactZombie, hit.point, Quaternion.LookRotation(hit.normal));
                Destroy(impactGo, 2f);
            }
            if (hit.transform.CompareTag("Metal"))
            {
                var impactGo = Instantiate(impactMetal, hit.point, Quaternion.LookRotation(hit.normal));
                Destroy(impactGo, 2f);
            }
            if (hit.transform.CompareTag("Dirt"))
            {
                var impactGo = Instantiate(impactDirt, hit.point, Quaternion.LookRotation(hit.normal));
                Destroy(impactGo, 2f);
            }
            if (hit.transform.CompareTag("Stone"))
            {
                var impactGo = Instantiate(impactStone, hit.point, Quaternion.LookRotation(hit.normal));
                Destroy(impactGo, 2f);
            }
            yield return new WaitForSeconds(0.30f);
            player.animator.SetBool("Shoot", false);

                yield return new WaitForSeconds(2.5f);        
        }
   
        isShooting = false;
    }

    // Update is called once per frame
    private void Update()
    {
        if (Time.timeScale == 0)
        {
            return;
            
        }


        if (bullet != null&& Time.timeScale ==0.01f)
        {
            float step = 400f * Time.deltaTime;
            bullet.transform.LookAt(hitpoint);
            //  bullet.GetComponent<Rigidbody>().AddForce(transform.forward*step);
            bullet.transform.position = Vector3.MoveTowards(bullet.transform.position, hitpoint, step);
            if (Vector3.Distance(bullet.transform.position, hitpoint) <=0f)
            {
                targetController.TakeDamage(damageKill);
                Destroy(bullet.gameObject);
                player.enabled = true;
                Time.timeScale = 1f;
                WeaponCamera.enabled = true;
                playerCamera.enabled = true;
                targetManager.SetActive(true);
                scopeManager.enabled = true;
                audioSource.pitch = 1f;
                canvas.enabled = true;
            }
        }


#if !MOBILE_INPUT
        if (Input.GetMouseButtonDown(0) && !isShooting&& holder.enabled)
        {
            StartCoroutine("Shoot");
        }
#endif
    }
}
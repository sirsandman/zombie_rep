﻿using UnityEngine;

public class DestroyAfterTime : MonoBehaviour
{
    // Use this for initialization
    private void Start()
    {
        Destroy(gameObject, 10f);
    }

    // Update is called once per frame
    private void Update()
    {
    }
}
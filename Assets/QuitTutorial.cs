﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitTutorial : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
	    Time.timeScale = 0;
	}


    public void ReturnToGame()
    {
        Time.timeScale = 1;
        this.gameObject.SetActive(false);
    }

	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown("space")|| Input.GetKeyDown("escape"))
	    {
	        ReturnToGame();
	    }
    }
}

﻿using UnityEngine;
using UnityEngine.AI;

public class TargetController : MonoBehaviour
{
    public float health = 50;
    public string name;
    private Animator anim;
    private WanderingAI aiController;
    private bool notDead=false;
    private AudioSource audioSource;
    [SerializeField] private AudioClip deathClip;
    [SerializeField] private GameObject SphereMinimap;
    void AddTagRecursively(Transform trans, string tag)
    {
        SphereMinimap.layer = 14;
        trans.gameObject.tag = tag;
        if (trans.GetChildCount() > 0)
            foreach (Transform t in trans)
                AddTagRecursively(t, tag);
    }

    public void DieExplosion()
    {
        if (notDead)
        {
            return;
        }
        audioSource.PlayOneShot(deathClip);
        AddTagRecursively(this.transform, "Zombie");

        gameObject.tag = "Zombie";
        notDead = true;
        anim.SetBool("Death", true);
        aiController.enabled = false;
        this.GetComponent<NavMeshAgent>().enabled = false;
    }

    public void TakeDamage(float dmg)
    {
        health -= dmg;
        if (health >= 0f)
        {
            anim.SetTrigger("Hit");
        }
    }

    private void Die()
    {
        AddTagRecursively(this.transform, "Zombie");

        gameObject.tag = "Zombie";
        audioSource.PlayOneShot(deathClip);
        notDead = true;
        anim.SetBool("Death",true);
        aiController.enabled = false;
        this.GetComponent<NavMeshAgent>().enabled = false;
    }

    // Use this for initialization
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        aiController = GetComponent<WanderingAI>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (health <= 0f&&!notDead)
        {
            Die();
        }
    }
}
﻿using UnityEngine;
using UnityEngine.AI;

public class WanderingAI : MonoBehaviour
{
    public float AnimatorSpeedOffset;
    public bool FireAffects;
    public bool FlareAffects;
    public GameObject[] flareArray;
    public GameObject[] GrenadeArray;
    public bool MeatAffects;
    public GameObject[] meatArray;
    public float wanderRadius;
    public float wanderTimer;
    private NavMeshAgent agent;
    private Animator anim;
    private GameObject closest;
    private Transform target;
    private Vector3 targetPosition;
    private float timer;
    private Transform startTransform;
    private bool runfrom=false;
    private float curDistance;
    public float distanceToObject;
    private AudioSource audioSource;
    private bool eating=false;
    [SerializeField] private GameObject Hostage;
    [SerializeField] private AudioClip eattingAudioClip;
    public Vector3 RandomNavmeshLocation(float radius)
    {
        var randomDirection = Random.insideUnitSphere * radius;
        randomDirection += transform.position;
        NavMeshHit hit;
        var finalPosition = Vector3.zero;
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
        {
            finalPosition = hit.position;
        }
        return finalPosition;
    }

    private void FindClosestTarget(string tagOfObject)
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag(tagOfObject);
        var distance = 40f;
        var position = transform.position;

        foreach (var go in gos)
        {
            var diff = go.transform.position - position;
            curDistance = diff.sqrMagnitude;
           // if (curDistance < distance)
           if (Vector3.Distance(go.transform.position , transform.position)<=50f)
            {
                closest = go;
                distance = curDistance;
            }
        }
        if (closest != null)
        {
            targetPosition = closest.transform.position;
        }
    }

    private void FindNewPoint()
    {
       // var newPos = RandomNavmeshLocation(wanderRadius);
        closest = gameObject;
        agent.SetDestination(Hostage.transform.position);

        timer = 0;
    }

    private void MoveToClosest(string tagOfObject)
    {
       
            FindClosestTarget(tagOfObject);
            agent.destination = targetPosition;
        


    }
    public void RunFrom(GameObject runfromObject)
    {        
        
        // store the starting transform
        startTransform = transform;
        //temporarily point the object to look away from the player
        transform.rotation = Quaternion.LookRotation(transform.position - runfromObject.transform.position);
        //Then we'll get the position on that rotation that's multiplyBy down the path (you could set a Random.range
        // for this if you want variable results) and store it in a new Vector3 called runTo
        Vector3 runTo = transform.position + transform.forward*10f;

        transform.position = startTransform.position;
        transform.rotation = startTransform.rotation;
        //Debug.Log("runTo = " + runTo);
        //So now we've got a Vector3 to run to and we can transfer that to a location on the NavMesh with samplePosition.
        NavMeshHit hit;    // stores the output in a variable called hit
        // 5 is the distance to check, assumes you use default for the NavMesh Layer name
        NavMesh.SamplePosition(runTo, out hit, 5, 1 << NavMesh.GetAreaFromName("Walkable"));       
        // And get it to head towards the found NavMesh position
        agent.SetDestination(hit.position);
    }
    // Use this for initialization
    private void Start()
    {
        Hostage = GameObject.FindWithTag("Hostage");
        agent = GetComponent<NavMeshAgent>();
        timer = wanderTimer;
        anim = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        anim.speed = agent.speed * AnimatorSpeedOffset;
    }

    // Update is called once per frame
    private void Update()
    {
        meatArray = GameObject.FindGameObjectsWithTag("Meat");
        flareArray = GameObject.FindGameObjectsWithTag("Flare");
        GrenadeArray = GameObject.FindGameObjectsWithTag("Grenade");
        foreach (var meat in meatArray)
        {
            distanceToObject = Vector3.Distance(meat.transform.position, transform.position);
            if (meatArray.Length != 0 && MeatAffects && Vector3.Distance(meat.transform.position,transform.position) <= 20f&&!runfrom)
            {
                MoveToClosest("Meat");
                if (Vector3.Distance(meat.transform.position, transform.position) <= 1f&&!eating)
                {
                    audioSource.PlayOneShot(eattingAudioClip);
                    anim.SetBool("Eating", true);
                    eating = true;
                    
                }
            }
          
        }

        if (meatArray.Length == 0)
        {
            anim.SetBool("Eating", false);
            eating = false;
        }
        if (flareArray.Length == 0)
        {
            runfrom = false;
        }

        if (GrenadeArray.Length == 0)
        {
            runfrom = false;
        }
        foreach (var flare in flareArray)
        {
            if (Vector3.Distance(flare.transform.position, transform.position) <= 20f)
            {
                if (flareArray.Length != 0 & FlareAffects && !runfrom)
                {
                    MoveToClosest("Flare");
                }
                else if (flareArray.Length != 0 & !FlareAffects)
                {
                    RunFrom(flare);
                    runfrom = true;
                }
              
            }
        }
        foreach (var grenade in GrenadeArray)
        {
            if (Vector3.Distance(grenade.transform.position, transform.position) <= 20f)
            {
                if (GrenadeArray.Length != 0 && FireAffects && !runfrom)
                {
                    MoveToClosest("Grenade");
                }
                else if (GrenadeArray.Length != 0 && !FireAffects)
                {
                    RunFrom(grenade);
                    runfrom = true;
                }
               
            }
        }
        if (closest == null)
        {
            timer = 35;
        }

        timer += Time.deltaTime;

        if (timer >= wanderTimer&&!runfrom)
        {
            FindNewPoint();
        }
    }
}
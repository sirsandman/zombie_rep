using System.Collections;
using UnityEngine;


    public class FpsCounter : MonoBehaviour
    {
        private float minFps = 999;
        private float fps = 999;
        private float fpsSum = 0;
        private int fpsSumCount = 0;
        private GameObject[] zombies;
        private float avgFps
        {
            get
            {
                if (fpsSumCount > 0)
                {
                    return fpsSum / fpsSumCount;
                }
                else
                {
                    return 0;
                }
            }
        }

        private void Start()
        {
            StartCoroutine(ResetFpsCoroutine());
            zombies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var zombie in zombies)
            {
                Destroy(zombie);
            }
    }

        private void Update()
        {
        zombies= GameObject.FindGameObjectsWithTag("Enemy");
        fps = 1 / Time.deltaTime;
            if (fps < minFps)
            {
                minFps = fps;
            }
            fpsSum += fps;
            fpsSumCount++;
        }

        private void OnGUI()
        {
            OnGUILogger.Log(0, string.Format("FPS: {0}", fps));
            OnGUILogger.Log(1, string.Format("min FPS: {0}", minFps));
            OnGUILogger.Log(2, string.Format("avg FPS: {0}", avgFps));
            OnGUILogger.Log(3, string.Format("Number of Zombies: {0}", zombies.Length));
    }

        private IEnumerator ResetFpsCoroutine()
        {
            while (true)
            {
                yield return new WaitForSeconds(2f);
                minFps = fps;
                fpsSum = 0;
                fpsSumCount = 0;
            }
        }
    }


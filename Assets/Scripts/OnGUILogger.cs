using UnityEngine;


    public static class OnGUILogger
    {
        private static bool inited = false;
        private static GUIStyle style;
        private static int sizeRatio = 35;
        private static int fontSize;

        private static void Init()
        {
            if (!inited)
            {
                fontSize = Screen.height / sizeRatio;
                style = new GUIStyle();
                style.normal.textColor = Color.white;
                style.fontSize = fontSize;
                inited = true;
            }
        }

        public static void Log(int vertPosition, string text)
        {
            Init();
            GUI.Label(new Rect(0, vertPosition * fontSize, 100, 100), text, style);
        }
    }


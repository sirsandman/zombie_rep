﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Spawner : MonoBehaviour
{
    //set custom range for random position
    public int radiusToSpawn = 10;
    [SerializeField]
    private List<GameObject> zombies;
    //gameobject for spawning 
    public GameObject[] spawnObj;

    public float[] ZombieTypesSpeed;
    public int[] ZombieTypes;
    private NavMeshAgent agent;

    public Vector3 GetRandomPointOnMesh()
    {
        Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * 50f;
        randomDirection += transform.position;
        NavMeshHit hit;
        NavMesh.SamplePosition(randomDirection, out hit, 50f, NavMesh.GetAreaFromName("Spawnable"));

        if (float.IsInfinity(hit.position.x) || float.IsInfinity(hit.position.y) || float.IsInfinity(hit.position.z))
            return GetRandomPointOnMesh();

        return hit.position;
    }

    public Vector3 RandomNavmeshLocation()
    {
        NavMeshTriangulation navMeshData = NavMesh.CalculateTriangulation();

        // Pick the first indice of a random triangle in the nav mesh
        int t = Random.Range(0, navMeshData.indices.Length - 3);

        // Select a random point on it
        Vector3 point = Vector3.Lerp(navMeshData.vertices[navMeshData.indices[t]], navMeshData.vertices[navMeshData.indices[t + 1]], Random.value);
        Vector3.Lerp(point, navMeshData.vertices[navMeshData.indices[t + 2]], Random.value);
        NavMeshHit hit;

        return point;      

    }

    private void SpawnObject(int i)
    {
        var o = Instantiate(spawnObj[i], GetRandomPointOnMesh(), Quaternion.identity);
        zombies.Add(o);
        agent = o.GetComponent<NavMeshAgent>();
        agent.speed = ZombieTypesSpeed[i];
        agent.Warp(o.transform.position);
    }

    public void DestroyZombies()
    {
        foreach (var zombieGameObject in zombies)
        {
            Destroy(zombieGameObject);
        }
        zombies.Clear();
    }


    public void Spawn()
    {    
        for (var i = 0; i < spawnObj.Length; i++)
        {
            for (var j = 0; j < ZombieTypes[i]; j++)
            {
                SpawnObject(i);
            }
        }
    }
}
﻿using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    // The different Enemy levels
    public enum EnemyLevels
    {
        Easy,
        Medium,
        Hard,
        Boss
    }

    //----------------------------------
    // All the Enums
    //----------------------------------
    // Spawn types
    public enum SpawnTypes
    {
        Normal,
        Once,
        Wave,
        TimedWave
    }

    public GameObject BossEnemy;

    //----------------------------------
    // Enemy Prefabs
    //----------------------------------
    public GameObject EasyEnemy;
    //---------------------------------
    // End of the Enums
    //---------------------------------

    // Enemy level to be spawnedEnemy
    public EnemyLevels enemyLevel = EnemyLevels.Easy;

    public GameObject HardEnemy;
    public GameObject MediumEnemy;
    public bool Spawn = true;

    public SpawnTypes spawnType = SpawnTypes.Normal;
    //----------------------------------
    // End of Enemy Prefabs
    //----------------------------------

    //----------------------------------
    // Enemies and how many have been created and how many are to be created
    //----------------------------------
    public int totalEnemy = 10;

    //Wave controls
    public int totalWaves = 5;

    // timed wave controls
    public float waveTimer = 30.0f;

    private int numEnemy;
    private int numWaves;

    private int spawnedEnemy;
    //----------------------------------
    // End of Enemy Settings
    //----------------------------------

    // The ID of the spawner
    private int SpawnID;

    //----------------------------------
    // Different Spawn states and ways of doing them
    //----------------------------------
    private bool waveSpawn;

    public EnemySpawner()
    {
        TimeTillWave = 0.0f;
    }

    //disable the spawner based on spawnerID
    public void disableSpawner(int sID)
    {
        if (SpawnID == sID)
        {
            Spawn = false;
        }
    }

    //enable the spawner based on spawnerID
    public void enableSpawner(int sID)
    {
        if (SpawnID == sID)
        {
            Spawn = true;
        }
    }

    // Enable the spawner, useful for trigger events because you don't know the spawner's ID.
    public void enableTrigger()
    {
        Spawn = true;
    }

    // Call this function from the enemy when it "dies" to remove an enemy count
    public void killEnemy(int sID)
    {
        // if the enemy's spawnId is equal to this spawnersID then remove an enemy count
        if (SpawnID == sID)
        {
            numEnemy--;
        }
    }

    // Draws a cube to show where the spawn point is... Useful if you don't have a object that show the spawn point
    private void OnDrawGizmos()
    {
        // Sets the color to red
        Gizmos.color = Color.red;
        //draws a small cube at the location of the gam object that the script is attached to
        Gizmos.DrawCube(transform.position, new Vector3(0.5f, 0.5f, 0.5f));
    }

    // spawns an enemy based on the enemy level that you selected
    private void spawnEnemy()
    {
        // To check which enemy prefab to instantiate
        if (enemyLevel == EnemyLevels.Easy)
        {
            // Checks to see if there is a gameobject in the easy enemy var
            if (EasyEnemy != null)
            {
                // spawns the enemy
                var Enemy = Instantiate(EasyEnemy, gameObject.transform.position, Quaternion.identity);
                // calls a function on the enemy that applies the spawner's ID to the enemy
            }
            else
            {
                //Shows a debug message if there is no prefab
                Debug.Log("ERROR: No easy enemy Prefab loaded");
            }
        }
        else if (enemyLevel == EnemyLevels.Medium)
        {
            // Checks to see if there is a gameobject in the medium enemy var
            if (MediumEnemy != null)
            {
                // spawns the enemy
                var Enemy = Instantiate(MediumEnemy, gameObject.transform.position, Quaternion.identity);
                // calls a function on the enemy that applies the spawner's ID to the enemy
                Enemy.SendMessage("setName", SpawnID);
            }
            else
            {
                //Shows a debug message if there is no prefab
                Debug.Log("ERROR: No medium enemy Prefab loaded");
            }
        }
        else if (enemyLevel == EnemyLevels.Hard)
        {
            // Checks to see if there is a gameobject in the hard enemy var
            if (HardEnemy != null)
            {
                // spawns the enemy
                var Enemy = Instantiate(HardEnemy, gameObject.transform.position, Quaternion.identity);
                // calls a function on the enemy that applies the spawner's ID to the enemy
                Enemy.SendMessage("setName", SpawnID);
            }
            else
            {
                //Shows a debug message if there is no prefab
                Debug.Log("ERROR: No hard enemy Prefab loaded");
            }
        }
        else if (enemyLevel == EnemyLevels.Boss)
        {
            // Checks to see if there is a gameobject in the boss enemy var
            if (BossEnemy != null)
            {
                // spawns the enemy
                var Enemy = Instantiate(BossEnemy, gameObject.transform.position, Quaternion.identity);
                // calls a function on the enemy that applies the spawner's ID to the enemy
                Enemy.SendMessage("setName", SpawnID);
            }
            else
            {
                //Shows a debug message if there is no prefab
                Debug.Log("ERROR: No boss enemy Prefab loaded");
            }
        }
        // Increase the total number of enemies spawned and the number of spawned enemies
        numEnemy++;
        spawnedEnemy++;
    }
    //----------------------------------
    // End of Different Spawn states and ways of doing them
    //----------------------------------

    private void Start()
    {
        // sets a random number for the id of the spawner
        SpawnID = Random.Range(1, 500);
    }

    private void Update()
    {
        if (Spawn)
        {
            // Spawns enemies everytime one dies
            if (spawnType == SpawnTypes.Normal)
            {
                // checks to see if the number of spawned enemies is less than the max num of enemies
                if (numEnemy < totalEnemy)
                {
                    // spawns an enemy
                    spawnEnemy();
                }
            }
            // Spawns enemies only once
            else if (spawnType == SpawnTypes.Once)
            {
                // checks to see if the overall spawned num of enemies is more or equal to the total to be spawned
                if (spawnedEnemy >= totalEnemy)
                {
                    //sets the spawner to false
                    Spawn = false;
                }
                else
                {
                    // spawns an enemy
                    spawnEnemy();
                }
            }
            //spawns enemies in waves, so once all are dead, spawns more
            else if (spawnType == SpawnTypes.Wave)
            {
                if (numWaves < totalWaves + 1)
                {
                    if (waveSpawn)
                    {
                        //spawns an enemy
                        spawnEnemy();
                    }
                    if (numEnemy == 0)
                    {
                        // enables the wave spawner
                        waveSpawn = true;
                        //increase the number of waves
                        numWaves++;
                    }
                    if (numEnemy == totalEnemy)
                    {
                        // disables the wave spawner
                        waveSpawn = false;
                    }
                }
            }
            // Spawns enemies in waves but based on time.
            else if (spawnType == SpawnTypes.TimedWave)
            {
                // checks if the number of waves is bigger than the total waves
                if (numWaves <= totalWaves)
                {
                    // Increases the timer to allow the timed waves to work
                    TimeTillWave += Time.deltaTime;
                    if (waveSpawn)
                    {
                        //spawns an enemy
                        spawnEnemy();
                    }
                    // checks if the time is equal to the time required for a new wave
                    if (TimeTillWave >= waveTimer)
                    {
                        // enables the wave spawner
                        waveSpawn = true;
                        // sets the time back to zero
                        TimeTillWave = 0.0f;
                        // increases the number of waves
                        numWaves++;
                        // A hack to get it to spawn the same number of enemies regardless of how many have been killed
                        numEnemy = 0;
                    }
                    if (numEnemy >= totalEnemy)
                    {
                        // diables the wave spawner
                        waveSpawn = false;
                    }
                }
                else
                {
                    Spawn = false;
                }
            }
        }
    }

    // returns the Time Till the Next Wave, for a interface, ect.
    public float TimeTillWave { get; private set; }
}
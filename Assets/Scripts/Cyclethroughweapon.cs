﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cyclethroughweapon : MonoBehaviour {
    private int selectedWeapon = 0;
    public GameObject WeaponHolder;
    void Start()
    {
        SelectWeapon(0);
    }
    void Update()
    {

        if (Input.GetKeyDown("tab"))
        {
            selectedWeapon++;
            if (selectedWeapon == WeaponHolder.transform.childCount)
            {
                selectedWeapon = 0;
            }
            SelectWeapon(selectedWeapon);
        }
    }
    void SelectWeapon(int index)
    {
        for (int i = 0; i < WeaponHolder.transform.childCount; i++)    
     {
            if (i == index)
            {
                WeaponHolder.transform.GetChild(i).gameObject.SetActiveRecursively(true);
            }
            else
            {
                WeaponHolder.transform.GetChild(i).gameObject.SetActiveRecursively(false);
            }
        }
    }
}

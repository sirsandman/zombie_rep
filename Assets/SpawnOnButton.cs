﻿using UnityEngine;

[AddComponentMenu("Spawn/Spawn On Button")]
public class SpawnOnButton : MonoBehaviour
{
    public string buttonName = "g";
    public GameObject objectToSpawn;

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(buttonName))
        {
            Instantiate(objectToSpawn, transform.position, transform.rotation);
        }
    }
}
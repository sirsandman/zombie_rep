﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowMouse : MonoBehaviour
{
    [SerializeField]
    private GameObject _settingsPanel;

    private bool pauseEnabled=false;
    // Use this for initialization
    void Start () {
	    Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ToggleCanvasGroupActive();
            

        }
    }
    public void ToggleCanvasGroupActive()
    {
        // This will set the canvas group to active if it is inactive OR set it to inactive if it is active
        _settingsPanel.gameObject.SetActive(!_settingsPanel.gameObject.activeSelf);
        //check if game is already paused
        if (pauseEnabled == true)
        {
            //unpause the game
            pauseEnabled = false;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1;
        }
        //else if game isn't paused, then pause it
        else if (pauseEnabled == false)
        {
            pauseEnabled = true;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0;
        }
    }
    public void HideCursor()
    {
        
        Cursor.visible = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using Invector.CharacterController;
using UnityEngine;

public class ThrowingController : MonoBehaviour {
    private bool throwing;
    [SerializeField] private GameObject renderPathGranade;
    public GameObject throwable;
    private int lastClicked;
    public vThirdPersonController player;
    private Cyclethroughweapon Holder;
    // Use this for initialization
    void Start ()
    {
        Holder = GetComponentInParent<Cyclethroughweapon>();
    }
    private void Throw()
    {
        if (!throwing&& Holder.enabled)
        {
            StartCoroutine("ThrowingCourutine");
        }
    }
 


    void OnEnable()
    {
        if (renderPathGranade.activeInHierarchy)
        {
            renderPathGranade.SetActive(false);
        }
    }

    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            Throw();
        }
    }
    private IEnumerator ThrowingCourutine()
    {
        throwing = true;
  
            lastClicked++;
            if (lastClicked == 1)
            {
                renderPathGranade.SetActive(true);
            }
            else if (lastClicked == 2)
            {
                player._rigidbody.isKinematic = true;
                player.animator.SetTrigger("Throw");
                renderPathGranade.SetActive(false);
            yield return new WaitForSeconds(0.8f);
            Instantiate(throwable, renderPathGranade.transform.position,
                    renderPathGranade.transform.rotation);

                lastClicked = 0;
            }
        yield return new WaitForSeconds(0.6f);
        player._rigidbody.isKinematic = false;
        
        throwing = false;
    }
}

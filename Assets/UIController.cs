﻿using UnityEngine;

public class UIController : MonoBehaviour
{
    public float hSliderValue;

    [SerializeField] private GameObject Overlay;

    // Use this for initialization
    private void OnGUI()
    {
        hSliderValue = GUI.HorizontalSlider(new Rect(940, 135 + Mathf.Sin(hSliderValue * -.0175f) * 30, 243, 30),
            hSliderValue, 0.0f, 180.0f);
        GUILayout.Label("This text makes just space");
    }

    // Update is called once per frame
    private void Start()
    {
    }
}
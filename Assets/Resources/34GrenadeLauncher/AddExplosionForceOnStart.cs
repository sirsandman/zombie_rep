﻿using UnityEngine;

public class AddExplosionForceOnStart : MonoBehaviour
{
    public GameObject Fire;
    public float force = 200.0f;
    public ForceMode forceMode;
    public float radius = 5.0f;
    public float upwardsModifier;
    private AudioSource audioSource;
    [SerializeField] private AudioClip explosionAudioClip;

    // Use this for initialization
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        
        GameObject fireGameobject;
        fireGameobject = Instantiate(Fire, transform.position, transform.rotation);
        audioSource.PlayOneShot(explosionAudioClip);
        foreach (var col in Physics.OverlapSphere(transform.position, radius))
        {
            if (col.GetComponent<Rigidbody>() != null)
            {
                col.GetComponent<Rigidbody>()
                    .AddExplosionForce(force, transform.position, radius, upwardsModifier, forceMode);
            }

            if (col.GetComponent<TargetController>() != null)
            {
                col.GetComponent<TargetController>().DieExplosion();
            }
        }
    }
}
﻿using UnityEngine;

public class GibOnCollide : MonoBehaviour
{
    public GameObject gib;

    private void OnCollisionEnter()
    {
        Instantiate(gib, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
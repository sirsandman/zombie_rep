﻿using UnityEngine;

public class AddForceOnStart : MonoBehaviour
{
    public float force = 100.0f;
    public ForceMode forceMode;

    // Use this for initialization
    private void Start()
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * force, forceMode);
    }

    void OnCollisionEnter(Collision collision)
    {
        transform.SetParent(collision.gameObject.transform);
       // this.GetComponent<Rigidbody>().isKinematic = true;
    }

}
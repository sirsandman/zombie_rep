﻿using UnityEngine;

public class BallisticTargeting : MonoBehaviour
{
    public class BallisticInfo
    {
        public float highAngle;
        public float lowAngle;
    }

    public bool debugMode;
    public Transform target;

    public GameObject targetHUD1;
    public GameObject targetHUD2;
    public Transform targetIndicator1;
    public Transform targetIndicator2;

    public float velocity = 10.0f; //Note: NOT LIVE TWEAKABLE because of caching

    private float v;
    private float vHyperCubed;
    private float vSquared;

    private BallisticInfo CalculateTrajectoryAngles()
    {
        var targetVector = target.position - transform.position;
        var height = targetVector.y;
        targetVector.y = 0.0f;

        var x = targetVector.magnitude;
        var y = height;
        var g = Mathf.Abs(Physics.gravity.y);

        var underTheRoot = vHyperCubed - g * (g * x * x + 2 * y * vSquared);

        if (underTheRoot < 0.0f)
        {
            return null;
        }

        var root = Mathf.Sqrt(underTheRoot);

        var top1 = vSquared + root;
        var top2 = vSquared - root;
        var bottom = g * x;

        var angle1 = Mathf.Atan2(bottom, top1);
        var angle2 = Mathf.Atan2(bottom, top2);

        var returnValue = new BallisticInfo();
        returnValue.lowAngle = angle1 * Mathf.Rad2Deg;
        returnValue.highAngle = angle2 * Mathf.Rad2Deg;

        return returnValue;
    }

    // Use this for initialization
    private void Start()
    {
        v = velocity;
        vSquared = v * v;
        vHyperCubed = vSquared * vSquared;
    }

    // Update is called once per frame
    private void Update()
    {
        var info = CalculateTrajectoryAngles();

        if (info != null)
        {
            targetHUD1.SetActive(true);
            targetHUD2.SetActive(true);
            targetIndicator1.localEulerAngles = new Vector3(-info.lowAngle, 0.0f, 0.0f);
            targetIndicator2.localEulerAngles = new Vector3(-info.highAngle, 0.0f, 0.0f);
            if (debugMode)
            {
                transform.localRotation = Quaternion.LookRotation(target.position - transform.position);
                Camera.main.transform.localEulerAngles = new Vector3(-info.highAngle, 0.0f, 0.0f);
            }
        }
        else
        {
            targetHUD1.SetActive(false);
            targetHUD2.SetActive(false);
        }
    }
}
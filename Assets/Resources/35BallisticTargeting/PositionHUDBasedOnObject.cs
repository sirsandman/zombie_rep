﻿using UnityEngine;

public class PositionHUDBasedOnObject : MonoBehaviour
{
    public Transform target;

    // Update is called once per frame
    private void Update()
    {
        transform.position = Camera.main.WorldToScreenPoint(target.position);
    }
}
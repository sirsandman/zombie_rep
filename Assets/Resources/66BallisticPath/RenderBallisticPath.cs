﻿using UnityEngine;

public class RenderBallisticPath : MonoBehaviour
{
    public GameObject explosionDisplay;
    public float initialVelocity = 10.0f;
    public LayerMask layerMask = -1;
    public float maxTime = 10.0f;
    public float timeResolution = 0.02f;

    private GameObject explosionDisplayInstance;

    private LineRenderer lineRenderer;

    // Use this for initialization
    private void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    private void Update()
    {
        var velocityVector = transform.forward * initialVelocity;

        lineRenderer.SetVertexCount((int) (maxTime / timeResolution));

        var index = 0;

        var currentPosition = transform.position;

        for (var t = 0.0f; t < maxTime; t += timeResolution)
        {
            lineRenderer.SetPosition(index, currentPosition);

            RaycastHit hit;

            if (Physics.Raycast(currentPosition, velocityVector, out hit, velocityVector.magnitude * timeResolution,
                layerMask))
            {
                lineRenderer.SetVertexCount(index + 2);

                lineRenderer.SetPosition(index + 1, hit.point);

                if (explosionDisplay != null)
                {
                    if (explosionDisplayInstance != null)
                    {
                        explosionDisplayInstance.SetActive(true);
                        explosionDisplayInstance.transform.position = hit.point;
                    }
                    else
                    {
                        explosionDisplayInstance = Instantiate(explosionDisplay, hit.point, Quaternion.identity);
                        explosionDisplayInstance.transform.parent = transform;
                        explosionDisplayInstance.SetActive(true);
                    }
                }

                break;
            }
            if (explosionDisplayInstance != null)
            {
                explosionDisplayInstance.SetActive(false);
            }

            currentPosition += velocityVector * timeResolution;
            velocityVector += Physics.gravity * timeResolution;
            index++;
        }
    }
}
﻿using UnityEngine;
using UnityEngine.UI;

public class RadialSlider : MonoBehaviour
{
    private bool isPointerDown = false;
    [SerializeField] private Camera MainCamera;
    private Text text;

    public void Update()
    {
        if (GetComponent<Image>().fillAmount != MainCamera.fieldOfView / 100f)
        {
            GetComponent<Image>().fillAmount = MainCamera.fieldOfView / 100f;
            text.text = MainCamera.fieldOfView.ToString();
            GetComponent<Image>().color = Color.Lerp(Color.green, Color.red, MainCamera.fieldOfView / 100f);
        }
    }

    private void Start()
    {
        text = GetComponentInChildren<Text>();
        text.text = MainCamera.fieldOfView.ToString();
    }
}
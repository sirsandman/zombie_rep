﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWeaponController : MonoBehaviour
{
    public GameObject ActualWeapon;

    public GameObject UIFade;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    if (ActualWeapon == null)
	    {
	        return;	        
	    }
	    if (ActualWeapon.activeInHierarchy)
	    {
	        UIFade.SetActive(false);
	    }
	    else
	    {
	        UIFade.SetActive(true);
	    }
	}
}

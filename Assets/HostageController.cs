﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostageController : MonoBehaviour
{
    private AudioSource audioSource;
    [SerializeField]
    private AudioClip shriek;
	// Use this for initialization
	void Start ()
	{
	    audioSource = GetComponent<AudioSource>();
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            audioSource.PlayOneShot(shriek);
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}

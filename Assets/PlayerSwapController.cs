﻿using System.Collections;
using System.Collections.Generic;
using Invector.CharacterController;
using UnityEngine;

public class PlayerSwapController : MonoBehaviour
{
    [SerializeField] private vThirdPersonInput[] players;
    [SerializeField] private GameObject[] playersGameObject;
    [SerializeField] private Cyclethroughweapon[] weapons;
    [SerializeField] private GameObject Path;
    [SerializeField] private GameObject[] UIplayerFade;
    private int currentPlayer;
	// Use this for initialization
	void Start ()
	{
	    currentPlayer = 1;
	    EnablePlayer(0);
        DisablePlayer(1);
	    DisablePlayer(2);
	    DisablePlayer(3);
    }

    void EnablePlayer(int i)
    {
        Camera.main.fieldOfView = 60f;
        UIplayerFade[i].SetActive(false);
         playersGameObject[i].GetComponent<Rigidbody>().isKinematic = false;
        playersGameObject[i].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
        vThirdPersonCamera.instance.target = playersGameObject[i].transform;
        vThirdPersonCamera.instance.Init();
        players[i].enabled = true;
        weapons[i].enabled = true;
        playersGameObject[i].GetComponent<vThirdPersonController>().enabled = true;

    }

    void DisablePlayer(int i)
    {
        if (playersGameObject[i] == null)
        {
            return;
        }
        UIplayerFade[i].SetActive(true);
        playersGameObject[i].GetComponent<vThirdPersonController>().animator.SetFloat("InputHorizontal", 0f);
        playersGameObject[i].GetComponent<vThirdPersonController>().animator.SetFloat("InputVertical", 0f);
        playersGameObject[i].GetComponent<vThirdPersonController>().animator.SetBool("IsGrounded", true);
        playersGameObject[i].GetComponent<vThirdPersonController>().enabled = false;
        if (playersGameObject[i].GetComponent<vThirdPersonController>().isGrounded)
        {
            playersGameObject[i].GetComponent<Rigidbody>().isKinematic = true;
        }
        playersGameObject[i].GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;
        players[i].enabled = false;
        weapons[i].enabled = false;
    }

	// Update is called once per frame
	void Update () {

        

	    if (Input.GetKeyDown("1") && currentPlayer != 1 && playersGameObject[0] !=null)
	    {
	        Path.SetActive(false);
            EnablePlayer(0);
	        currentPlayer=1;
            for (int i = 0; i < players.Length; i++)
	        {
	            if (i != 0)
	            {
                    DisablePlayer(i);
	            }
	        }
	    }

	    if (Input.GetKeyDown("2") && currentPlayer != 2 && playersGameObject[1] != null)
	    {
	        currentPlayer = 2;
	        Path.SetActive(false);
            EnablePlayer(1);
            for (int i = 0; i < players.Length; i++)
	        {
	            if (i != 1)
	            {
	                DisablePlayer(i);
                }
	        }
	    }

        if (Input.GetKeyDown("3") && currentPlayer != 3 && playersGameObject[2] != null)
	    {
	        
            Path.SetActive(false);
            EnablePlayer(2);
	        currentPlayer = 3;
            for (int i = 0; i < players.Length; i++)
	        {
	            if (i != 2)
	            {
	                DisablePlayer(i);
                }
	        }
	    }

	    if (Input.GetKeyDown("4") && currentPlayer != 4 && playersGameObject[3] != null) 
	    {
	        currentPlayer = 4;
	        Path.SetActive(false);
            EnablePlayer(3);
            for (int i = 0; i < players.Length; i++)
	        {
	            if (i != 3)
	            {
	                DisablePlayer(i);
                }
	        }
	    }
    }
}

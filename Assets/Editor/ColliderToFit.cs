﻿using UnityEditor;
using UnityEngine;

public class ColliderToFit : MonoBehaviour
{
    [MenuItem("My Tools/Collider/Fit to Children")]
    private static void FitToChildren()
    {
        foreach (var rootGameObject in Selection.gameObjects)
        {
            if (!(rootGameObject.GetComponent<Collider>() is BoxCollider))
            {
                continue;
            }

            var hasBounds = false;
            var bounds = new Bounds(Vector3.zero, Vector3.zero);

            for (var i = 0; i < rootGameObject.transform.childCount; ++i)
            {
                var childRenderer = rootGameObject.transform.GetChild(i).GetComponent<Renderer>();
                if (childRenderer != null)
                {
                    if (hasBounds)
                    {
                        bounds.Encapsulate(childRenderer.bounds);
                    }
                    else
                    {
                        bounds = childRenderer.bounds;
                        hasBounds = true;
                    }
                }
            }

            var collider = (BoxCollider) rootGameObject.GetComponent<Collider>();
            collider.center = bounds.center - rootGameObject.transform.position;
            collider.size = bounds.size;
        }
    }
}
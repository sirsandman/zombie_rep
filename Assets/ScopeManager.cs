﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class ScopeManager : MonoBehaviour
{
    public bool isScoped=false;
    public Camera MainCamera;
    public float orthoZoomSpeed = 0.2f; // The rate of change of the orthographic size in orthographic mode.
    public float perspectiveZoomSpeed = 0.2f; // The rate of change of the field of view in perspective mode.
    public GameObject WeaponCamera;
    [SerializeField] private Animator animator;
    private Vector3 firstpoint; //change type on Vector3
    [SerializeField] private Image moveJoystick;
    private readonly float normalFOV = 60f;
    [SerializeField] private GameObject scopedOverlay;
    private Vector3 secondpoint;
    [SerializeField] private GameObject SniperRifle;
    [SerializeField] private GameObject switchPanel;
    private double xAngle; //angle for axes x for rotation
    private double xAngTemp = 0f; //temp variable for angle
    private double yAngle;
    private double yAngTemp = 0f;
    [SerializeField] private float zoomValue = 20f;
    private Cyclethroughweapon holder;
    public void ScopeInAndOut()
    {
        xAngle = MainCamera.transform.eulerAngles.x;
        yAngle = MainCamera.transform.eulerAngles.y;
        isScoped = !isScoped;
       // animator.SetBool("isScoped", isScoped);
        if (isScoped)
        {
            StartCoroutine(onScoped());
        }
        else
        {
            onUnscoped();
        }
    }

    private IEnumerator onScoped()
    {
        yield return new WaitForSeconds(0.15f);
        //scopedOverlay.SetActive(true);
        WeaponCamera.SetActive(false);
        moveJoystick.enabled = false;
        switchPanel.SetActive(false);
        MainCamera.fieldOfView = zoomValue;
    }

    private void onUnscoped()
    {
        //zoomValue = MainCamera.fieldOfView;
        scopedOverlay.SetActive(false);
        WeaponCamera.SetActive(true);
        switchPanel.SetActive(true);
        MainCamera.fieldOfView = normalFOV;
        moveJoystick.enabled = true;
    }

    // Use this for initialization
    private void Start()
    {
        holder = GetComponentInParent<Cyclethroughweapon>();
        animator = SniperRifle.GetComponent<Animator>();
        xAngle = 0f;
        yAngle = 0f;
    }

    // Update is called once per frame
    private void Update()
    {
#if !MOBILE_INPUT

        if (Input.GetMouseButtonDown(1)&& holder.enabled)
        {
            ScopeInAndOut();
        }
        if (isScoped)
        {
            var fov = Camera.main.fieldOfView;
         //   fov += Input.GetAxis("Mouse ScrollWheel") * 10f;
            fov = Mathf.Clamp(fov, 2f, 100f);
            Camera.main.fieldOfView = fov;
        }

#endif

        // If there are two touches on the device...
        if (Input.touchCount == 2 && isScoped)
        {
            // Store both touches.
            var touchZero = Input.GetTouch(0);
            var touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            var touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            var touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            var prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            var touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            var deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            // If the camera is orthographic...
            if (MainCamera.orthographic)
            {
                // ... change the orthographic size based on the change in distance between the touches.
                MainCamera.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;
                xAngle = 0f;
                yAngle = 0f;
                // Make sure the orthographic size never drops below zero.
                MainCamera.orthographicSize = Mathf.Max(MainCamera.orthographicSize, 0.1f);
            }
            else
            {
                // Otherwise change the field of view based on the change in distance between the touches.
                MainCamera.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;

                // Clamp the field of view to make sure it's between 0 and 180.
                MainCamera.fieldOfView = Mathf.Clamp(MainCamera.fieldOfView, 2f, 100f);
            }
        }
    }
}
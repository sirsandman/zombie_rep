﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZombieSpawnController : MonoBehaviour
{
    private int _numberOfZombie = 0;
    public int ZombieType;
    private float _speedOfZombie;
    [SerializeField] private Spawner spawner;
    [SerializeField]
    InputField input;
    [SerializeField]
    Slider _slider;
    [SerializeField] private Text _sliderText;
    // Use this for initialization
    void OnEnable()
    {
     
    }


    public void Spawn()
    {
        spawner.ZombieTypes[ZombieType] = _numberOfZombie;
        spawner.ZombieTypesSpeed[ZombieType] = _speedOfZombie;
       
    }
	// Update is called once per frame
	void Update () {
	    _numberOfZombie = int.Parse(input.text);
	    _speedOfZombie = _slider.value;
	    _sliderText.text = "Speed: " + "\n" + _slider.value.ToString("F2");
	}
}

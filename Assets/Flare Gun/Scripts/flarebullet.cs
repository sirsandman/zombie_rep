﻿using System.Collections;
using UnityEngine;

public class flarebullet : MonoBehaviour
{
    public AudioClip flareBurningSound;
    public float flareTimer = 9;

    private Light flarelight;
    private AudioSource flaresound;
    private bool myCoroutine;
    private ParticleRenderer smokepParSystem;
    private readonly float smooth = 2.4f;

    private IEnumerator flareLightoff()
    {
        myCoroutine = true;
        yield return new WaitForSeconds(flareTimer);
        myCoroutine = false;
    }

    // Use this for initialization
    private void Start()
    {
        StartCoroutine("flareLightoff");

        GetComponent<AudioSource>().PlayOneShot(flareBurningSound);
        flarelight = GetComponent<Light>();
        flaresound = GetComponent<AudioSource>();
        smokepParSystem = GetComponent<ParticleRenderer>();

        Destroy(gameObject, flareTimer + 1f);
    }

    // Update is called once per frame
    private void Update()
    {
        if (myCoroutine)

        {
            flarelight.intensity = Random.Range(0.5f, 1.0f);
        }
        else

        {
            flarelight.intensity = Mathf.Lerp(flarelight.intensity, 0f, Time.deltaTime * smooth);
            flarelight.range = Mathf.Lerp(flarelight.range, 0f, Time.deltaTime * smooth);
            flaresound.volume = Mathf.Lerp(flaresound.volume, 0f, Time.deltaTime * smooth);
            smokepParSystem.maxParticleSize =
                Mathf.Lerp(smokepParSystem.maxParticleSize, 0f, Time.deltaTime * 5); //switched to legacy
        }
    }
}
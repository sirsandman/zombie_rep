﻿using UnityEngine;

public class swayc : MonoBehaviour
{
    public GameObject gun;
    public float moveAmount = 1f;
    public float moveSpeed = 2f;
    public Vector3 newGunPosition;
    private Vector3 defaultPosition;
    private float moveOnX;
    private float moveOnY;

    private void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 150, 30), "Press 'R' to reload");
    }

    // Use this for initialization
    private void Start()
    {
        defaultPosition = transform.position;
    }

    // Update is called once per frame
    private void Update()
    {
        moveOnX = Input.GetAxis("Mouse X") * Time.deltaTime * moveAmount;

        moveOnY = Input.GetAxis("Mouse Y") * Time.deltaTime * moveAmount;

        newGunPosition = new Vector3(defaultPosition.x + moveOnX, defaultPosition.y + moveOnY, defaultPosition.z);

        gun.transform.localPosition =
            Vector3.Lerp(gun.transform.localPosition, newGunPosition, moveSpeed * Time.deltaTime);
    }
}
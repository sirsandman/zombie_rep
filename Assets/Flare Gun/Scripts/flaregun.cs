﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class flaregun : MonoBehaviour
{
    public Transform barrelEnd;
    public int bulletSpeed = 2000;
    private Cyclethroughweapon holder;
    public Rigidbody flareBullet;
    public AudioClip flareShotSound;
    public GameObject muzzleParticles;
    public AudioClip noAmmoSound;
    public AudioClip reloadSound;
    public bool shooting;

    void Start()
    {
        holder = GetComponentInParent<Cyclethroughweapon>();
    }
    IEnumerator ShootCourutine()
    {
        shooting = true;
        GetComponent<Animation>().CrossFade("Shoot");
        GetComponent<AudioSource>().PlayOneShot(flareShotSound);

        Rigidbody bulletInstance;
        bulletInstance =
            Instantiate(flareBullet, barrelEnd.position, barrelEnd.rotation); //INSTANTIATING THE FLARE PROJECTILE

        bulletInstance.AddForce(Camera.main.transform.forward * bulletSpeed); //ADDING FORWARD FORCE TO THE FLARE PROJECTILE

        Instantiate(muzzleParticles, barrelEnd.position, barrelEnd.rotation); //INSTANTIATING THE GUN'S MUZZLE SPARKS	
        yield return new WaitForSeconds(0.5f);
        shooting = false;
    }
    private bool IsPointerOverUIObject()
    {
        var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
    public void Shoot()
    {
        if (!shooting && holder.enabled)
        {
            StartCoroutine("ShootCourutine");
        }
    }

    // Update is called once per frame
    private void Update()
    {
#if !MOBILE_INPUT

        if (Input.GetButtonDown("Fire1")&& !IsPointerOverUIObject())
        {
            Shoot();
        }

#endif
    }
}
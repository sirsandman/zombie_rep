﻿using System.Collections;
using UnityEngine;

public class ThrowingManager : MonoBehaviour
{
    public string buttonName = "g";
    public GameObject objectToSpawn;
    public GameObject[] ThrowableGameObjects;
    public GameObject[] VisualGameObjects;
    private int countThrowable;
    [SerializeField] private flaregun flaregun;
    private int i = 1;
    private int lastClicked;
    private bool throwing;

    [SerializeField] private GameObject renderPathGranade;

    [SerializeField] private ScopeManager scopeManager;
    [SerializeField] private SniperController sniper;

    public void ChangeDown()
    {
        if (i > 0 && !scopeManager.isScoped)
        {
            i--;
            VisualGameObjects[i + 1].SetActive(false);
            VisualGameObjects[i].SetActive(true);
        }
    }

    public void ChangeUp()
    {
        if (i < VisualGameObjects.Length - 1 && !scopeManager.isScoped)
        {
            i++;
            VisualGameObjects[i - 1].SetActive(false);
            VisualGameObjects[i].SetActive(true);
        }
    }

    public void ThrowAndShootButton()
    {
        if (i == 0)
        {
            flaregun.Shoot();
        }

        if (i == 1)
        {
            sniper.ShotMobile();
        }

        if (i == 2 || i == 3)
        {
            Throw();        
        }
    }

    // Use this for initialization


    private IEnumerator ThrowingCourutine()
    {
        throwing = true;
        if (i == 2)
        {
            countThrowable = 0;
        }
        if (i == 3)
        {
            countThrowable = 1;
        }
        if (VisualGameObjects[2].activeInHierarchy || VisualGameObjects[3].activeInHierarchy)
        {
            lastClicked++;
            if (lastClicked == 1)
            {
                renderPathGranade.SetActive(true);
            }
            else if (lastClicked == 2)
            {
                Instantiate(ThrowableGameObjects[countThrowable], renderPathGranade.transform.position,
                    renderPathGranade.transform.rotation);
                renderPathGranade.SetActive(false);
                lastClicked = 0;
            }
        }
        yield return new WaitForSeconds(0.6f);
        throwing = false;
    }

    private void Throw()
    {
        if (!throwing)
        {
            StartCoroutine("ThrowingCourutine");
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (Time.timeScale == 0)
            return;
#if MOBILE_INPUT

       if (i == 0 || i == 1)
        {
            renderPathGranade.SetActive(false);
        }
#endif
        if (!scopeManager.isScoped&& sniper.bullet == null && !VisualGameObjects[0].GetComponent<flaregun>().shooting)
        {
#if !MOBILE_INPUT
            if (VisualGameObjects[0].activeInHierarchy || VisualGameObjects[1].activeInHierarchy)
            {
                renderPathGranade.SetActive(false);
                lastClicked = 0;
            }

            if (Input.GetKeyDown(buttonName))
            {
                Throw();
            }

            if (Input.GetKeyDown("1"))
            {
                foreach (var b in VisualGameObjects)
                {
                    b.SetActive(false);
                }
                VisualGameObjects[0].SetActive(true);
            }
            if (Input.GetKeyDown("2"))
            {
                foreach (var b in VisualGameObjects)
                {
                    b.SetActive(false);
                }
                VisualGameObjects[1].SetActive(true);
            }
            if (Input.GetKeyDown("3"))
            {
                foreach (var b in VisualGameObjects)
                {
                    b.SetActive(false);
                }
                countThrowable = 0;
                VisualGameObjects[2].SetActive(true);
            }
            if (Input.GetKeyDown("4"))
            {
                foreach (var b in VisualGameObjects)
                {
                    b.SetActive(false);
                }
                countThrowable = 1;
                VisualGameObjects[3].SetActive(true);
            }
#endif

        }
    }
}
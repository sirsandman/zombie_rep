﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LavaController : MonoBehaviour
{
    [SerializeField] private List<GameObject> players;
    // Use this for initialization
    void Start()
    {

    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player" && players.Count > 1)
        {
            Destroy(col.gameObject);
            players.Remove(col.gameObject);
        }
        else if (col.gameObject.tag == "Player"&& players.Count ==1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
    // Update is called once per frame
    void Update()
    {

    }
}
